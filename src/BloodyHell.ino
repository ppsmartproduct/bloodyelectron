/*
 * Project BloodyHell
 * Description: Smart Blood Container System
 * Author: Jakkrit S.
 * Date: Revision 1: 8/11/18
 */
#include "Particle.h"
#include "DS18B20.h"
#include "AssetTrackerRK.h"
#include "Adafruit_SSD1306.h"
#include "blynk.h"

#define PARTICLE_KEEPALIVE 15 // DTAC 15 sec. AIS 45 sec.

String DEVICE_NAME = "BOX_ID_001";

#define RELAY_PIN D6
#define DS18B20_PIN D2

////////////////////////////////////////////////////////////////////////
// Blynk Setup
#define BLYNK_IP IPAddress(188,166,206,43)
#define BLYNK_PRINT Serial  // Serial output for debug prints
#define BLYNK_DEBUG
// Set Blynk hertbeat interval.
// Each heartbeat uses ~90 bytes of data.
#define BLYNK_HEARTBEAT 60
// Blynk Auth Token
char auth[] = "059f483636b945a1a92eacdb33c3718c";
BlynkTimer timer;

#define BLYNK_VIRTUAL_PIN_SLIDE_TEMP_INPUT V1
#define BLYNK_VIRTUAL_PIN_LAT              V2
#define BLYNK_VIRTUAL_PIN_LON              V3
#define BLYNK_VIRTUAL_PIN_GPSMSG           V4
#define BLYNK_VIRTUAL_PIN_TEMPC            V5
#define BLYNK_VIRTUAL_PIN_TEMPH            V6
#define BLYNK_VIRTUAL_PIN_BATTVOLTAGE      V7
#define BLYNK_VIRTUAL_PIN_BATTPERCENTAGE   V8
#define BLYNK_VIRTUAL_PIN_MAP              V9
#define BLYNK_VIRTUAL_PIN_RELAY_LOCK       V10
#define BLYNK_VIRTUAL_PIN_ZEBRA            V12
#define BLYNK_VIRTUAL_PIN_TWITTER          V13

WidgetMap gpsMap(BLYNK_VIRTUAL_PIN_MAP);

int slideValue = 0;
////////////////////////////////////////////////////////////////////////
// Adafruit_SSD1306 setup
// Oled Reset Pin D4
#define OLED_RESET D4
////////////////////////////////////////////////////////////////////////
Adafruit_SSD1306 oled(OLED_RESET);
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
// DS18B20 setup
// DS18B20 Pin D2 - Electron Pin D2
////////////////////////////////////////////////////////////////////////
DS18B20 ds18b20(DS18B20_PIN, true);
double celcius;
double fahrenheit;
const int MAX_RETRY = 4;
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// AssetTracker
int transmittingData = 1;
// 1 - both Particle.publish and Serial.Print
// 0 - just Serial.print
AssetTracker t = AssetTracker();
FuelGauge fuel; // battery state
float lat;
float lon;
bool hasGPSFix;
float battVoltage;
float battPercentage;
String messageGPSBlynk;
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// PARTICLE FUNCTION Declare
int gpsPublish(String command);
int batteryStatus(String command);
////////////////////////////////////////////////////////////////////////

// Particle Time Sync
unsigned long lastSync;

void setup() {
  pinMode(DS18B20_PIN, INPUT); // DS18B20 Temp Sensor
  pinMode(RELAY_PIN, OUTPUT); // Relay

  oled.begin(SSD1306_SWITCHCAPVCC, 0x3C); // 0x3D for 128x32 display
  oled.clearDisplay();

  delay(2000); // Cold start

  Time.zone(7);
  lastSync = Time.now();

  Blynk.begin(auth, BLYNK_IP);

  if(Blynk.connected()) {
    Blynk.syncAll();
  }

  t.begin();
  t.gpsOn();

  Serial.begin(9600);

  Particle.keepAlive(PARTICLE_KEEPALIVE);

  // Particle Functions Declare
  Particle.function("batt", batteryStatus);
  Particle.function("gps", gpsPublish);

  // Run Functions on Timer
  timer.setInterval(1500L, readFromDS18B20);
  timer.setInterval(1500L, displayInfoOnOled);
  timer.setInterval(60000L, getGPSinfo);
  timer.setInterval(60000L, getDeviceStatus);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // run blynk timer
  timer.run();
  /* sync the clock once a day on Particle Cloud */
      	if (Time.now() > lastSync + 86400) {
      		Particle.syncTime();
      		lastSync = Time.now();
      	}
  Blynk.run();

  t.updateGPS();

  oled.display();
}

////////////////////////////////////////////////////////////////////////
void getGPSinfo() {
  //String pubAccel = String::format("%d,%d,%d", t.readX(), t.readY(), t.readZ());
  //Serial.println(pubAccel);
  //Particle.publish("A", pubAccel, 60, PRIVATE);
  // TODO: USE WAKE_ON_MOVE
  Serial.println(t.preNMEA());
  if(t.gpsFix()) {
    lat = t.readLat();
    lon = t.readLon();
    hasGPSFix = t.gpsFix();
    if(hasGPSFix) {
      messageGPSBlynk = "GPS OK";
    }
    Particle.publish("GPS", t.readLatLon(), 60, PRIVATE);
    Serial.println(t.readLatLon());
  } else {
    Serial.println('Waiting for GPS');
    messageGPSBlynk = "Waiting for GPS";
  }
  writeGPSInfoToBlynk(lat, lon, messageGPSBlynk);
}

////////////////////////////////////////////////////////////////////////
void readFromDS18B20() {
  int i = 0;
  do {
    celcius = ds18b20.getTemperature();
  } while (!ds18b20.crcCheck() && MAX_RETRY > i++);

  if (i < MAX_RETRY) {
    fahrenheit = ds18b20.convertToFahrenheit(celcius);
    Serial.print(celcius);
    Serial.print(" C : ");
    Serial.print(fahrenheit);
    Serial.println(" F");
  } else {
    celcius = fahrenheit = NAN;
    Serial.println("Invalid Reading Temp");
  }
  writeTemperatureToBlynk(celcius, fahrenheit);
}
////////////////////////////////////////////////////////////////////////

void getDeviceStatus() {
  battVoltage = fuel.getVCell();
  battPercentage = fuel.getSoC();
  writeDeviceInfoToBlynk(battVoltage, battPercentage);
}

////////////////////////////////////////////////////////////////////////
// displayInfoOnOled
void displayInfoOnOled() {
  if(isnan(celcius) || isnan(fahrenheit)) {
    oled.clearDisplay();
    oled.setTextColor(WHITE);
    oled.setTextSize(2);
    oled.setCursor(5, 0);
    oled.print('Temp Error!');
    return;
  }
  oled.clearDisplay();
  oled.setTextColor(WHITE);
  oled.setTextSize(2);
  oled.setCursor(0, 0);
  oled.print(celcius);
  oled.print(" C");
  oled.setCursor(0, 22);
  oled.print(fahrenheit);
  oled.print(" F");
  oled.setCursor(0, 42);
  oled.print("SET: ");
  Serial.println(slideValue);
  oled.print(slideValue);
  oled.print(" C");
}
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
// Particle Functions
int gpsPublish(String command) {
  if (t.gpsFix()) {
    Particle.publish("GPS", t.readLatLon(), 60, PRIVATE);
    // reset delay counter for manual publish;
    // lastPublish = millis();
    return 1;
  } else {
    return 0;
  }
}

int batteryStatus(String command) {
  Particle.publish("Batt",
          "v:" + String::format("%.2f",fuel.getVCell()) +
          ",c:" + String::format("%.2f",fuel.getSoC()),
          60, PRIVATE
          );
  // if there's more than 10% of the battery left, then return 1
  if (fuel.getSoC()>10){ return 1;}
  // if you're running out of battery, return 0
  else { return 0;}
}
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
// Helper Functions
void writeTemperatureToBlynk(double tempC, double tempF) {
  Blynk.virtualWrite(BLYNK_VIRTUAL_PIN_TEMPC, tempC);
  Blynk.virtualWrite(BLYNK_VIRTUAL_PIN_TEMPH, tempF);
}

void writeGPSInfoToBlynk(float lattitude, float longitude, String gpsMessage) {
  Blynk.virtualWrite(BLYNK_VIRTUAL_PIN_LAT, lattitude);
  Blynk.virtualWrite(BLYNK_VIRTUAL_PIN_LON, longitude);
  Blynk.virtualWrite(BLYNK_VIRTUAL_PIN_GPSMSG, gpsMessage);
  gpsMap.location(0, lattitude, longitude, DEVICE_NAME);
}

void writeDeviceInfoToBlynk(float battery_voltage, float battery_percentage) {
  Blynk.virtualWrite(BLYNK_VIRTUAL_PIN_BATTVOLTAGE, battery_voltage);
  Blynk.virtualWrite(BLYNK_VIRTUAL_PIN_BATTPERCENTAGE, battery_percentage);
}

////////////////////////////////////////////////////////////////////////
// Blynk
////////////////////////////////////////////////////////////////////////
//
//  ZeRGBa Widget
// Attach a ZeRGBa widget (mode: Merge) to the Virtual pin 2
// - and control the built-in RGB led!
BLYNK_WRITE(BLYNK_VIRTUAL_PIN_ZEBRA) {
        int r = param[0].asInt();
        int g = param[1].asInt();
        int b = param[2].asInt();
        if (r > 0 || g > 0 || b > 0) {
                RGB.control(true);
                RGB.color(r, g, b);
        } else {
                RGB.control(false);
        }
}

// Attach a Button widget (mode: Push) to the Virtual pin 13 - and send sweet tweets!
BLYNK_WRITE(BLYNK_VIRTUAL_PIN_TWITTER) {
        if (param.asInt() == 1) { // On button down...
                // Tweeting!
                // Note:
                //   We allow 1 tweet per minute for now.
                //   Twitter doesn't allow identical subsequent messages.
                Blynk.tweet("BloodyHell is Online!");

                // Pushing notification to the app!
                // Note:
                //   We allow 1 notification per minute for now.
                Blynk.notify("Currently Streaming Data!");
        }
}

BLYNK_WRITE(BLYNK_VIRTUAL_PIN_SLIDE_TEMP_INPUT) {
  slideValue = param.asInt();
}

BLYNK_WRITE(BLYNK_VIRTUAL_PIN_RELAY_LOCK) {
  int relayState = param.asInt();
  if(relayState == 1) {
    digitalWrite(RELAY_PIN, HIGH);
  } else {
    digitalWrite(RELAY_PIN, LOW);
  }
}
